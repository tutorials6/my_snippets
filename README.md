# my_snippets

need a space to link to my snippets only
but to share them??
( hope i find a better way about that sharing )

#_____ template

[SKETCH_MIN](https://gitlab.com/snippets/1926918)

[SKETCH_EASY](https://gitlab.com/snippets/1926919)

[STATE_MIN](https://gitlab.com/snippets/1929137)

#_____ timer

[BASIC TIMER](https://gitlab.com/snippets/1926921)

[MULTI TIMER](https://gitlab.com/snippets/1927126)

[SCHEDULE TIMER](https://gitlab.com/snippets/1927129)

[SEVEN_SEGMENT_LOOK CLOCK](https://gitlab.com/snippets/1927666)

#_____ info

[SYS INFO](https://gitlab.com/snippets/1926924)

[FPS GRAPH stress test](https://gitlab.com/snippets/1927937)

[NUM INFO](https://gitlab.com/snippets/1926925)

#_____ my special keyboard operations Plus Plus 

[MWPP](https://gitlab.com/snippets/1926926)

[KPP](https://gitlab.com/snippets/1926927)

#_____ examples

[LOG](https://gitlab.com/snippets/1926920)

[CSV](https://gitlab.com/snippets/1926923)

[ball hits rectangle example](https://gitlab.com/snippets/1926928)

[canvas focus](https://gitlab.com/snippets/1926938)

[arduino USB catch CSV lines](https://gitlab.com/snippets/1928961)  

#_____ links

[p5.js GITLAB webpage BIOROCK design tool](https://biorock.gitlab.io/biorock/biorock.html)

[p5.js Hang / HandPan](https://editor.p5js.org/kll/sketches/feSN2S7Pr)

